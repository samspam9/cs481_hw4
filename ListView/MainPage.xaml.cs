﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Web;
using System.Net;
using Newtonsoft.Json;

namespace ListView
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(true)]
    public class Crypto
    {
        public string Name { get; set; }
        public string Symbol { get; set; }
        public float Price { get; set; }
        public float Volume24 { get; set; }
        public float ChangePercent { get; set; }
    }
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            generateList();
            cryptoListView.RefreshCommand = new Command(() => {
                generateList();
                cryptoListView.IsRefreshing = false;
            });
        }

        private void generateList()
        {
            dynamic rates = JsonConvert.DeserializeObject(getRates());
            var ratesList = rates.data;
            List<Crypto> cryptoList = new List<Crypto>();
            foreach (var rate in ratesList)
            {
                Console.WriteLine(rate);
                cryptoList.Add(new Crypto()
                {
                    Name = rate.name,
                    Symbol = rate.symbol,
                    Price = rate.quote.USD.price,
                    Volume24 = rate.quote.USD.volume_24h,
                    ChangePercent = rate.quote.USD.percent_change_1h
                });
            }
            cryptoListView.ItemsSource = cryptoList;
        }

        private string getRates()
        {
            var URL = new UriBuilder("https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest");

            var queryString = HttpUtility.ParseQueryString(string.Empty);
            queryString["start"] = "1";
            queryString["limit"] = "20";
            queryString["convert"] = "USD";

            URL.Query = queryString.ToString();

            var client = new WebClient();
            client.Headers.Add("X-CMC_PRO_API_KEY", "f923dd2e-f28d-492d-8a4c-ab84d6372206");
            client.Headers.Add("Accepts", "application/json");
            return client.DownloadString(URL.ToString());
        }

        private void View_Clicked(object sender, EventArgs e)
        {
            var menuitem = sender as MenuItem;
            if (menuitem != null)
            {
                var crypto = menuitem.BindingContext as Crypto;
                Navigation.PushAsync(new CryptoPage(crypto));
            }
        }
        private void Delete_Clicked(object sender, EventArgs e)
        {

        }
    }
}
