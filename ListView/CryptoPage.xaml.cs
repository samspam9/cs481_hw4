﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ListView
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CryptoPage : ContentPage
    {
        public CryptoPage(Crypto crypto)
        {
            InitializeComponent();
            name.Text = crypto.Name;
            symbol.Text = crypto.Symbol;
            price.Text += crypto.Price.ToString() + " USD";
            percentChange.Text += crypto.ChangePercent.ToString() + " %";
            volume.Text += (crypto.Volume24 / 1000000).ToString() + "M";
        }
    }
}